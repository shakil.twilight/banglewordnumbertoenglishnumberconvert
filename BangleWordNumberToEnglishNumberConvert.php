<?php

namespace App\Repository;

class BangleWordNumberToEnglishNumberConvert
{
    /**
     * @var mixed
     */
    protected $word = '';
    /**
     * @var false|int|null
     */
    protected $bangleWordCount = 0;
    /**
     * @var string[][]
     */
    protected $bangleWordList = [];
    private $number = null;
    private $isBangle = false;
    private $originalWord = null;

    /**
     * @param $word
     * @return $this
     */
    public function setWord($word): BangleWordNumberToEnglishNumberConvert
    {
        $this->word = $word;
        $this->originalWord = $word;

        return $this;
    }

    /**
     * @return $this
     */
    protected function hasBangleWordCheck(): BangleWordNumberToEnglishNumberConvert
    {
        // Regular expression pattern for Bengali words
        $pattern = '/\p{Bengali}+/u';
        $this->bangleWordCount = preg_match_all($pattern, $this->word, $matches);
        if ($this->bangleWordCount !== 0) {
            $this->isBangle = true;
        }
        $this->bangleWordList = $matches;

        return $this;
    }

    /**
     * @return array
     */
    public function convert(): array
    {
        $this->hasBangleWordCheck();

        if ($this->isBangle) {
            $this->bangleWordClean();
            $this->bangleWordToEngNumber();

            return $this->wordResponseFormat();
        }

        $this->removeSpecial();

        if (preg_match('/^[0-9]+$/', $this->word)) {
            $this->number = (int)$this->word;
            return $this->wordResponseFormat();
        }

        $this->convertTextualNumberToNumeric();

        if (is_null($this->number)) {
            $this->extractIntegerFromString();
        }

        return $this->wordResponseFormat();
    }


    /**
     * @return $this
     */
    protected function bangleWordClean(): BangleWordNumberToEnglishNumberConvert
    {
        //Remove any extra spaces from the input
        $this->word = preg_replace('/[^[:alnum:]ঀ-৾\s]/u', '', $this->word);
        $this->word = preg_replace('/[A-Za-z0-9]+/u', '', $this->word);
        $this->word = trim($this->word);

        return $this;
    }

    public function bangleWordToEngNumber()
    {
        $index = array_search($this->word, [
            'শূন্য', 'এক', 'দুই', 'তিন', 'চার', 'পাঁচ', 'ছয়', 'সাত', 'আট', 'নয়', 'দশ',
            'এগারো', 'বার', 'তের', 'চৌদ্দ', 'পনের', 'ষোল', 'সতের', 'আঠার', 'উনিশ', 'বিশ',
            'একুশ', 'বাইশ', 'তেইশ', 'চব্বিশ', 'পঁচিশ', 'ছাব্বিশ', 'সাতাশ', 'আটাশ', 'ঊনত্রিশ', 'ত্রিশ',
            'একত্রিশ', 'বত্রিশ', 'তেত্রিশ', 'চৌত্রিশ', 'পঁয়ত্রিশ', 'ছত্রিশ', 'সাঁইত্রিশ', 'আটত্রিশ', 'ঊনচল্লিশ',
            'চল্লিশ', 'একচল্লিশ', 'বিয়াল্লিশ', 'তেতাল্লিশ', 'চৌচল্লিশ', 'পঁয়তাল্লিশ', 'ছিচল্লিশ', 'সাতচল্লিশ',
            'আটচল্লিশ', 'ঊনপঞ্চাশ', 'পঞ্চাশ', 'একান্ন', 'বায়ান্ন', 'তিপ্পান্ন', 'চুয়ান্ন', 'পঞ্চান্ন', 'ছাপ্পান্ন', 'সাতান্ন',
            'আটান্ন', 'ঊনষাট', 'ষাট', 'একষট্টি', 'বাষট্টি', 'তিষট্টি', 'চৌষট্টি', 'পঁয়ষট্টি', 'ছিষট্টি', 'সাতষট্টি', 'আষট্টি',
            'ঊনসত্তর', 'সত্তর', 'একাত্তর', 'বায়াত্তর', 'তিয়াত্তর', 'চুয়াত্তর', 'পঁচাত্তর', 'ছিয়াত্তর', 'সাতাত্তর', 'আটাত্তর', 'ঊনআশি',
            'আশি', 'একাশি', 'বিরাশি', 'তিরাশি', 'চুরাশি', 'পঁচাশি', 'ছিষাশি', 'সাতাশি', 'আটাশি', 'ঊননব্বই', 'নব্বই',
            'একানব্বই', 'বিরানব্বই', 'তিরানব্বই', 'চুরানব্বই', 'পঁচানব্বই', 'ছিয়ানব্বই', 'সাতানব্বই', 'আটানব্বই', 'নিরানব্বই'
        ]);

        if ($index !== false) {
            $this->number = $index;
        } else {
            $this->convertBengaliToEnglishNumber();
        }
    }

    public function convertBengaliToEnglishNumber()
    {
        // Define an associative array to map Bengali numerals to English numerals
        $numberList = [
            '০' => '0',
            '১' => '1',
            '২' => '2',
            '৩' => '3',
            '৪' => '4',
            '৫' => '5',
            '৬' => '6',
            '৭' => '7',
            '৮' => '8',
            '৯' => '9'
        ];

        $this->number = strtr($this->word, $numberList);
        $this->removeBengaliWordsAndSpecialChars();
    }


    public function removeBengaliWordsAndSpecialChars()
    {
        // Define a regular expression pattern to match numeric characters
        $pattern = '/[^0-9]+/';

        // Use preg_replace to remove non-numeric characters
        $this->number = preg_replace($pattern, '', $this->number);
    }


    /**
     * @return void|null
     */
    public function convertTextualNumberToNumeric()
    {
        // Define a mapping for common textual numbers
        $textualNumbers = array(
            'zero' => 0,
            'one' => 1,
            'two' => 2,
            'three' => 3,
            'four' => 4,
            'five' => 5,
            'six' => 6,
            'seven' => 7,
            'eight' => 8,
            'nine' => 9,
            'ten' => 10,
            'eleven' => 11,
            'twelve' => 12,
            'thirteen' => 13,
            'fourteen' => 14,
            'fifteen' => 15,
            'sixteen' => 16,
            'seventeen' => 17,
            'eighteen' => 18,
            'nineteen' => 19,
            'twenty' => 20,
            'thirty' => 30,
            'forty' => 40,
            'fifty' => 50,
            'sixty' => 60,
            'seventy' => 70,
            'eighty' => 80,
            'ninety' => 90,
            'hundred' => 100,
            'thousand' => 1000,
            'million' => 1000000,
            'billion' => 1000000000
        );

        // Split the cleaned input into words
        $words = explode(' ', $this->word);

        // Initialize the result
        $result = 0;
        $currentNumber = 0;

        // Iterate through the words and accumulate the numeric value

        foreach ($words as $word) {
            if (isset($textualNumbers[$word])) {
                if ($word == 'hundred') {
                    // Handle 'hundred' as a multiplier
                    $currentNumber *= $textualNumbers[$word];
                } else {
                    // Accumulate the current number
                    $currentNumber += $textualNumbers[$word];
                }
            } else {
                // If a word is not in the mapping, return an error or handle it as needed
                return null;
            }
        }

        // Add the final result if it's not zero
        if ($currentNumber !== 0) {
            $result += $currentNumber;
        }

        $this->number = $result;
    }

    public function removeSpecial()
    {
        // Remove special characters and convert the input to lowercase
        $this->word = trim($this->word);
        $this->word = preg_replace('/[^a-zA-Z0-9\s]+/', '', $this->word);
    }

    public function extractIntegerFromString()
    {
        // Use a regular expression to match and extract the integer
        if (preg_match('/\d+/', $this->word, $matches)) {
            // Convert the matched number to an integer
            $this->word = $matches[0];
            $this->number = (int)$matches[0];
        }
    }


    /**
     * @return array
     */
    protected function wordResponseFormat(): array
    {
        return [
            'number' => $this->number,
            'isBangle' => $this->isBangle,
            'bangleWordCount' => $this->bangleWordCount,
            'originalWord' => $this->originalWord,
            'cleanupWord' => $this->word,
        ];
    }


}