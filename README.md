# BangleWordNumberToEnglishNumberConvert

## Introduction
The `BangleWordNumberToEnglishNumberConvert` class is designed to convert a given input word or string, which may contain Bengali words, into its corresponding numeric value in English. It provides methods to identify and handle Bengali words, clean the input, and perform the conversion.

## Table of Contents
- [Properties](#properties)
- [Methods](#methods)
- [Algorithm](#algorithm)
- [Usage](#usage)
- [License](#license)

## Properties
- `word`: The input word or string to be converted.
- `bangleWordCount`: Count of Bengali words found in the input.
- `bangleWordList`: List of Bengali words found in the input.
- `number`: The resulting numeric value.
- `isBangle`: Flag indicating if the input contains Bengali words.
- `originalWord`: The original input word.

## Methods
- `setWord(word)`: Set the input word and initialize other properties.
- `hasBangleWordCheck()`: Check if the input contains Bengali words.
- `convert()`: Main conversion method.
- `bangleWordClean()`: Clean the input by removing special characters and non-Bengali characters.
- `bangleWordToEngNumber()`: Convert Bengali words to English numerals.
- `convertBengaliToEnglishNumber()`: Convert Bengali numerals to English numerals.
- `removeSpecial()`: Remove special characters and spaces from the input.
- `wordResponseFormat()`: Format the result in an array.

## Algorithm
### `convert()`
1. Call `hasBangleWordCheck()` to determine if the input contains Bengali words.
2. If `isBangle` is true, call `bangleWordClean()` to clean the input.
3. Call `bangleWordToEngNumber()` to convert Bengali words to English numerals.
4. If `isBangle` is false, call `removeSpecial()` to remove special characters and spaces.
5. Check if the cleaned word is a numeric string.
  - If it is numeric, set the `number` property as an integer.
  - If it is not numeric, call `convertBengaliToEnglishNumber()` to handle textual numbers.
6. Call `wordResponseFormat()` to format the result and return it as an array.

### `bangleWordToEngNumber()`
1. Check if the cleaned word matches Bengali words in the predefined list.
2. If a match is found, set the `number` property to the corresponding numeric value.

### `convertBengaliToEnglishNumber()`
1. Define a mapping for Bengali numerals to English numerals.
2. Replace Bengali numerals in the word with their English equivalents using `strtr`.
3. Call `removeBengaliWordsAndSpecialChars()` to remove non-numeric characters.

### `removeBengaliWordsAndSpecialChars()`
1. Define a regular expression pattern to match non-numeric characters.
2. Use `preg_replace` to remove non-numeric characters from the word.

### `removeSpecial()`
1. Trim the word to remove leading and trailing spaces.
2. Use `preg_replace` to remove special characters and spaces from the word.

### `wordResponseFormat()`
1. Return an array containing:
  - `number`: The resulting numeric value.
  - `isBangle`: Whether the input contained Bengali words.
  - `bangleWordCount`: The count of Bengali words found.
  - `originalWord`: The original input word.
  - `cleanupWord`: The cleaned and processed word.

## Usage
To use this class, follow these steps:

```php
// Create an instance of BangleWordNumberToEnglishNumberConvert
$converter = new BangleWordNumberToEnglishNumberConvert();

// Set the input word
$converter->setWord("তিনটি সত্তর");

// Perform the conversion
$result = $converter->convert();

// Access the numeric value
$numericValue = $result['number'];
```

## Example Usage

To illustrate the functionality of the `BangleWordNumberToEnglishNumberConvert` class, let's explore how it handles various input texts:

 
<summary>Example 1: Converting English Words to Numbers</summary>

```php
$text = 'one';
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
//Output: [ 'number' => 1, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => 'one', 'cleanupWord' => 'one' ]

```

 <summary>Example 2: Converting English Words to Numbers</summary>

```php
$text = 'three';
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => 3, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => 'three', 'cleanupWord' => 'three' ]


```

 <summary>Example 3: Converting English Words to Numbers</summary>

```php
$text = 'nine hundred@';
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => 900, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => 'nine hundred@', 'cleanupWord' => 'nine hundred' ]

```

<summary>Example 4: Converting English Words to Numbers</summary>

```php
$text = 'invalid input';
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => null, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => 'invalid input', 'cleanupWord' => 'invalid input' ]

```


<summary>Example 5: Converting English Words to Numbers</summary>

```php
$text = 'invalid input';
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => null, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => 'invalid input', 'cleanupWord' => 'invalid input' ]

```

<summary>Example 6: Converting English Words to Numbers</summary>

```php
$text = " তিন ";
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => 3, 'isBangle' => true, 'bangleWordCount' => 1, 'originalWord' => ' তিন ', 'cleanupWord' => '৩০' ]


```

<summary>Example 7: Converting English Words to Numbers</summary>

```php
$text = " 234we";
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => 234, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => ' 234we', 'cleanupWord' => '234' ]

```

<summary>Example 8: Converting English Words to Numbers</summary>

```php
$text = " nine hundred@";
$result = (new \App\Repository\BangleWordNumberToEnglishNumberConvert())->setWord($text)->convert();
// Output: [ 'number' => 900, 'isBangle' => false, 'bangleWordCount' => 0, 'originalWord' => ' nine hundred@', 'cleanupWord' => 'nine hundred' ]

```
